unit Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  uniGUITypes, uniGUIAbstractClasses, uniGUIClasses, uniGUIRegClasses,
  uniGUIForm, uniEdit, uniButton, uniGUIBaseClasses, uniLabel, uniCheckBox,
  uniGroupBox;

type
  TLoginForm = class(TUniLoginForm)
    GroupBoxLogin: TUniGroupBox;
    LUserName: TUniLabel;
    LPassWord: TUniLabel;
    BtnLogin: TUniButton;
    EditUserName: TUniEdit;
    EditPassWord: TUniEdit;
    procedure BtnLoginClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function LoginForm: TLoginForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication;

function LoginForm: TLoginForm;
begin
  Result := TLoginForm(UniMainModule.GetFormInstance(TLoginForm));
end;

procedure TLoginForm.BtnLoginClick(Sender: TObject);
begin
  {登录验证通过之后返回mrOk,进入主界面Main,错误则返回mrCancel,退出登录}
  ModalResult := mrOk;
end;

initialization
  RegisterAppFormClass(TLoginForm);

end.

